# Targomo Test Challenge

# Targomo Test Challenge 

This is the README for the automation testing .

Automated testing by Nightwatch framework powered by Node.js and using W3C Webdriver (formerly Selenium).


Git Clone or Git Download

Start off by making the project directory


# Project Setup

mkdir testing

cd testing

mkdir screenshots

To install it run:

npm install


**To run the complete test suite:**

npm run test



# Configuration Files

**nightwatch.json**

**package.json**


## Setup Guides

Specific WebDriver setup guides can be found on the [nightwatchjs.org](http://nightwatchjs.org/gettingstarted)


### List of technologies :

**mocha** – 

**Nightwatch v1.0** 

**selenium** 




## ChromeDriver

for running tests against the Chrome browser;

 - download url: https://sites.google.com/a/chromium.org/chromedriver/downloads.

## GeckoDriver

 for running tests against the Mozilla Firefox browser;

 - download url: https://github.com/mozilla/geckodriver/releases.

## Selenium Standalone Server

allows managing multiple browser configurations in one place and also to make use of the Selenium Grid service;
the selenium server jar file selenium-server-standalone-3.x.x.jar can be downloaded from the Selenium releases page: https://selenium-release.storage.googleapis.com/index.html

**Install mocha**

 - Navigate to your project directory
 - Enter : npm install -g mocha
 - Enter : npm install request --sav

## Reports

  - Error_shots -  screenshots gets saved if a command fails
  

  
## ScreenShotS

  - Capture screenshot and saved in screenshot folder
  

## File to call the runner


 - nightwatch.js :  require('nightwatch/bin/runner.js');
 
## Nightwatch unit tests

The tests for Nightwatch are written using Mocha.



 
 
 
 
 
 
 