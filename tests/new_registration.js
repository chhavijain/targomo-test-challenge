const path = require('path')
const testUrl1 = 'https://account.targomo.com/signup'


describe('Automated Test Cases\n  ', function() {
    it(' TESTCASE 1 : User Login \n', function(browser) {
        browser
            .maximizeWindow()
            .url(testUrl1)
            .assert.urlContains('/signup')
            .click("button.mat-button:nth-child(6)")
            .assert.visible("#mat-input-8")
            .assert.visible("#mat-input-9")
            .setValue('#mat-input-8', "chhavijain1205@gmail.com", function() {
                        console.log(" \n  ► Enter your Email  ")
                    })
            .setValue('#mat-input-9', "Fonpit120587", function() {
                        console.log("  ► Enter your Password  ")
                    }) 
             .click('.mat-button-wrapper > div:nth-child(1) > div:nth-child(1)')
             .saveScreenshot("./screenshots/signup.png")
            })
    
     it('\n TESTCASE 2 : Test Sidebar Menu options Clickable\n', function(browser) {
                browser
                    .waitForElementPresent('mat-list-item.ng-star-inserted:nth-child(1) > div:nth-child(1) > div:nth-child(3) > span:nth-child(1)', 3000, function() {
                    console.log("  ►  TARGOMO Account > Dashboard : General  ")
                })   
                .click("#hs-eu-confirmation-button", function(){
                    console.log("\n ► Accept Cookies ")
                }) 
            .click("mat-list-item.mat-list-item:nth-child(2) > div:nth-child(1) > div:nth-child(3) > span:nth-child(1)", function(){
            console.log("\n ►  Billing : Clickable ")
            })
            .pause(2000)
            .click("mat-list-item.mat-list-item:nth-child(3) > div:nth-child(1) > div:nth-child(3)", function(){
             console.log("\n ►  Plan : Clickable ")
        })
            .pause(2000)
            .click("mat-list-item.mat-list-item:nth-child(4) > div:nth-child(1) > div:nth-child(3) > span:nth-child(1)", function(){
            console.log("\n ► Statistics : Clickable")
        })
            .pause(2000)
            .click("mat-list-item.mat-list-item:nth-child(5) > div:nth-child(1) > div:nth-child(3) > span:nth-child(1)", function(){
            console.log("\n ►  Communications : Clickable ")  
        })
            .pause(2000)
            .click(".minifier > mat-list-item:nth-child(1) > div:nth-child(1) > div:nth-child(3) > span:nth-child(1)", function(){
            console.log("\n ► Collapse sidebar ")   
        })
            .click(".minifier > mat-list-item:nth-child(1) > div:nth-child(1) > div:nth-child(3) > span:nth-child(1)"  )          
            .pause(2000)                    
        })
        
        it(' TESTCASE 3 : EDIT BILLING Information \n', function(browser) {
              browser
        .click("mat-list-item.mat-list-item:nth-child(2) > div:nth-child(1) > div:nth-child(3) > span:nth-child(1)")   
        .click("button.mat-button:nth-child(1) > span:nth-child(1)", function(){
        console.log("\n ►  Click on the button, Edit Billing Information ")
        })
        .clearValue('#mat-input-23')
       .setValue('#mat-input-23', "chhavi", function() {
            console.log("   ► Enter Billing Name  ")
        })
        .pause(1000)
        .clearValue('#mat-input-25')
        .setValue('#mat-input-25', "Steglitz", function() {
            console.log("   ► Enter Address Line1   ")
        })
        .clearValue('#mat-input-27')
        .setValue('#mat-input-27', "Berlin", function() {
            console.log("   ► Enter Billing City  ")
        })
        .clearValue('#mat-input-29')
        .setValue('#mat-input-29', "chhavi", function() {
            console.log("   ► Enter Billing Postal Code  ")
        })
        
        .click(".mat-raised-button > span:nth-child(1)", function(){
            console.log(" ►  Click on the button, Save Changes")
            console.log(" ►  Displays : New Billing Information Submitted  ")
            console.log(" ►  User Successfully edit the billing informtion ")    
        })
            .pause(2000)
           
            .end()
                      
    })
})